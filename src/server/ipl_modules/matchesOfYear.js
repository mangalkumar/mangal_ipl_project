
function matchesOfYear(matches, year){
    const matchesOfrequiredYear = matches.filter((match)=> {
        return match.season == year
      })

    return matchesOfrequiredYear
    
}    

module.exports = matchesOfYear