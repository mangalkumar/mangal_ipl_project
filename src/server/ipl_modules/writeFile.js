const fs = require('fs')

function writeFile(path, data){
  return new Promise((resolve, reject)=>{
    fs.writeFile(path, data, (error) => {
      if (error){
          return reject(error)
      }else{
          resolve('Data saved in Output folder');
  
      }
    })
  })
}

module.exports = writeFile