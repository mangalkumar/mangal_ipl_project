const csv = require('csvtojson')

function readCsvFile(filePath){
    return new Promise((resolve, reject)=>{
        csv().fromFile(filePath)
        .then((matches)=>{
            return resolve(matches)
        })
        .catch((error)=>{
            return reject(error)
        })
    })
}

module.exports = readCsvFile