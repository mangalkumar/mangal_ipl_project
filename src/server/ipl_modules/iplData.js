const readCsvFile = require('./readCsvFile')

async function getIplData(matchesFilePath, deliveriesFilePath){
    const matchesData = await readCsvFile(matchesFilePath)
    const deliveriesData = await readCsvFile(deliveriesFilePath)

    return [matchesData, deliveriesData]
}

module.exports = getIplData