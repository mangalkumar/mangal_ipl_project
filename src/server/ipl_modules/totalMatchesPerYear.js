
function totalMatchesPerYear(matches){
    return new Promise((resolve, reject)=>{
        try{
            const totalMatchesPerYearData = matches.reduce((accumulator, match)=>{
                if(accumulator[match['season']] == undefined){
                    accumulator[match['season']] = 1
                }else{
                    accumulator[match['season']] += 1
                }
                return accumulator
            },{})
            
            return resolve(totalMatchesPerYearData)
        }catch(error){
            return reject(error)
        }
    })
}

module.exports = totalMatchesPerYear