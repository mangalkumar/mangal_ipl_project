
function matchesWonPerYear(matches){

    return new Promise((resolve, reject)=>{
        try{
            let matchesWonPerYearData = matches.reduce((accumulator, match)=>{
                if(accumulator[match['season']] == undefined){
                    accumulator[match['season']] = {}
                    accumulator[match['season']][match['winner']] = 1
                }else{
                    if(accumulator[match['season']][match['winner']] == undefined){
                        accumulator[match['season']][match['winner']] = 1
                    }else{
                        accumulator[match['season']][match['winner']] += 1;
                    }
                }
                return accumulator
            },{})
            
            return resolve(matchesWonPerYearData)

        }catch(error){
            return reject(error)
        }
    })
}

module.exports = matchesWonPerYear
       