const matchesOfYear = require('./matchesOfYear')

function extraRunsPerTeam(matches, deliveries){
    return new Promise((resolve, reject)=>{
        try{
            const matches_2016 = matchesOfYear(matches, 2016)

            const runsPerTeam = deliveries.reduce((accumulator, delivery)=>{
                matches_2016.forEach(match => {
                    if(delivery.match_id == match.id && delivery.extra_runs > 0){
                    
                        if(accumulator[delivery.batting_team] == undefined){
                            accumulator[delivery.batting_team] = parseInt(delivery.extra_runs)
                        }else{
                            accumulator[delivery.batting_team] += parseInt(delivery.extra_runs);
                        }
                    }
                })
                return accumulator
            },{})
            
            return resolve(runsPerTeam)

        }catch(error){
            return  reject(error)
        }
    })
}

module.exports = extraRunsPerTeam