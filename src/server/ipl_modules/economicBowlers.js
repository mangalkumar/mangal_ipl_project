const matchesOfYear = require('./matchesOfYear')

function economicBowlers(matches, deliveries){
  return new Promise((resolve, reject)=>{
    try{

      const matches_2015 = matchesOfYear(matches, 2015)
    
        const bowlers = deliveries.reduce((accumulator, delivery)=>{
            matches_2015.forEach(match => {
              if(delivery.match_id == match.id && delivery.extra_runs > 0){
                
                if(accumulator[delivery.bowler] == undefined){
                  accumulator[delivery.bowler] = {}
                  accumulator[delivery.bowler]['extra_runs'] = parseInt(delivery.extra_runs)
                  accumulator[delivery.bowler]['overs'] = 1
                }else{
                  accumulator[delivery.bowler]['extra_runs'] += parseInt(delivery.extra_runs)
                  accumulator[delivery.bowler]['overs'] += 1
                }
              }
            })
            return accumulator
        },{})
    
        let bowlersEconomicRates = Object.keys(bowlers)
        .reduce((accumulator, bowler)=>{
          let currentBowler = {}
    
          currentBowler[bowler] = parseFloat(
              bowlers[bowler].extra_runs / bowlers[bowler].overs
            ).toFixed(2)
    
          accumulator.push(currentBowler)
    
          return accumulator
        }, [])
    
        bowlersEconomicRates.sort((a,b)=>{
          return parseInt(Object.values(b)[0]) -  parseInt(Object.values(a)[0])
        })
    
        return resolve(bowlersEconomicRates)
    
    }catch(err){
      reject(err)
    }
  })
}

module.exports = economicBowlers