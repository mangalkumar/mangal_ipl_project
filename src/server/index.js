
const economicBowlers = require('./ipl_modules/economicBowlers')
const extraRunsPerTeam = require('./ipl_modules/extraRunsPerTeam')
const matchesWonPerYear = require('./ipl_modules/matchesWonPerYear')
const totalMatches = require('./ipl_modules/totalMatchesPerYear')
const getIplData = require('./ipl_modules/iplData')

const writeFile = require('./ipl_modules/writeFile')

const matchesFilePath = '../data/matches.csv';
const deliveriesFilePath = '../data/deliveries.csv';

async function ipl(){

  try{
    const iplData = await getIplData(matchesFilePath, deliveriesFilePath)

    const matches = iplData[0]
    const deliveries = iplData[1]
  
    const bowlersEconomicRates = await economicBowlers(matches, deliveries)
    const TOP_ECONOMIC_BOWLERS_PATH = "../public/output/TopEconomicBowlers.json"
    const topTenEconomicBowlers = bowlersEconomicRates.slice(0,10)
    const topTenEconomicBowlersStringifiedData = JSON.stringify(topTenEconomicBowlers)
    const msg1 = await writeFile(TOP_ECONOMIC_BOWLERS_PATH, topTenEconomicBowlersStringifiedData)
    console.log(msg1 + " as TopEconomicBowlers.json")
  
    const extraRunsPerTeamData = await extraRunsPerTeam(matches, deliveries)
    const EXTRA_RUNS_PER_TEAM_PATH = "../public/output/ExtraRunsPerTeam.json"
    const extraRunsPerTeamStringifiedData = JSON.stringify(extraRunsPerTeamData)
    const msg2 = await writeFile(EXTRA_RUNS_PER_TEAM_PATH, extraRunsPerTeamStringifiedData)
    console.log(msg2 + " as ExtraRunsPerTeam.json")
  
    const matchesWonPerYearData = await matchesWonPerYear(matches)
    const MATCHES_WON_PER_YEAR_PATH = "../public/output/MatchesWonPerYear.json"
    const matchesWonPerYearStringifiedData = JSON.stringify(matchesWonPerYearData)
    const msg3 = await writeFile(MATCHES_WON_PER_YEAR_PATH, matchesWonPerYearStringifiedData)
    console.log(msg3 + " as MatchesWonPerYear.json")
  
    const totalMatchesData = await totalMatches(matches)
    const TOTAL_MATCHES_PATH = "../public/output/TotalMatches.json"
    const totalMatchesStringifiedData = JSON.stringify(totalMatchesData)
    const msg4 = await writeFile(TOTAL_MATCHES_PATH, totalMatchesStringifiedData)
    console.log(msg4 + " as TotalMatches.json")

  }catch(error){
    console.error(error)
  }
}

ipl()
